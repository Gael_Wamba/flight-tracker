const colors = require("tailwindcss/colors");

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      minWidth: {
        '32': '128px',
        '28': '112px',
        '24': '96px',
      },
      colors: {
        transparent: "transparent",
        black: colors.black,
        white: colors.white,
        arvd: {
          DEFAULT: "rgb(21 128 61)"
        },
        pdep: {
          DEFAULT: "#3867d6"
        },
        amber: {
          DEFAULT: "#a5b1c2"
        }
      },
    },
  },
  plugins: [],
}
