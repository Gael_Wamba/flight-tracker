import moment from "moment";
import { Airport } from "../FlightSearch/FlightSearch";
import ProgressBar from "../ProgressBar/ProgressBar";
import Logo from "../../assets/images/emirates-logo.png";

interface FlightRouteProps {
  flightRoute: {
    originActualAirportCode: string;
    destinationActualAirportCode: string;
    statusCode: string;
    flightPosition: number;
    departureTime: {
      actual: Date;
      schedule: Date;
    };
    arrivalTime: {
      actual: Date;
      schedule: Date;
    };
  };
  airports: Array<Airport>;
  airlineDesignator: string;
  flightNumber: string;
}
const FlightRoute = ({
  flightRoute,
  airports,
  airlineDesignator,
  flightNumber,
}: FlightRouteProps) => {
  let originAirport = airports.filter(
    (airport: Airport) => airport.code === flightRoute.originActualAirportCode
  )[0].name;
  let destinationAirport = airports.filter(
    (airport: Airport) =>
      airport.code === flightRoute.destinationActualAirportCode
  )[0].name;
  let statusCodeClass = "";
  let statusCodeText = "";
  switch (flightRoute.statusCode) {
    case "ARVD":
      statusCodeClass = "bg-arvd";
      statusCodeText = "Arrived";
      break;

    case "PDEP":
      statusCodeClass = "bg-pdep";
      statusCodeText = "Not departed";
      break;

    default:
      statusCodeClass = "bg-amber";
      statusCodeText = "On Route";
      break;
  }
  return (
    <div className="grid md:grid-cols-9 gap-2 mb-6">
      <div className="order-3 md:order-1 grid grid-cols-1 md:col-span-7 md:pr-4 md:border-r-2 md:border-gray-200 md:border-dashed">
        <div className="flex flex-row justify-between mb-2">
          <h1 className="text-lg font-bold">{originAirport}</h1>
          <h1 className="text-lg font-bold">{destinationAirport}</h1>
        </div>
        <div className="flex flex-row justify-between">
          <span className="text-xs">Departed</span>
          <span className="text-xs">Arrived</span>
        </div>
        <div className="flex flex-row justify-between">
          <h1 className="text-4xl font-bold">
            {moment(flightRoute.departureTime.actual).format("HH:mm")}
          </h1>
          <div className="hidden md:flex md:justify-center md:items-center">
            <ProgressBar progressPercentage={flightRoute.flightPosition} />
          </div>
          <h1 className="text-4xl font-bold">
            {moment(flightRoute.arrivalTime.actual).format("HH:mm")}
          </h1>
        </div>
        <div className="flex flex-row justify-between mb-2">
          <span className="text-xs">
            {moment(flightRoute.departureTime.actual).format("ddd D MMM")}
          </span>
          <span className="text-xs">
            {moment(flightRoute.arrivalTime.actual).format("ddd D MMM")}
          </span>
        </div>
        <div className="flex flex-row justify-between">
          <span className="text-xs">
            Scheduled Departure:{" "}
            {moment(flightRoute.departureTime.schedule).format("HH:mm")}
          </span>
          <span className="text-xs">
            Scheduled Arrival:{" "}
            {moment(flightRoute.arrivalTime.schedule).format("HH:mm")}
          </span>
        </div>
      </div>
      <div className="order-2 md:hidden flex md:justify-center md:items-center  my-6">
        <ProgressBar progressPercentage={flightRoute.flightPosition} />
      </div>
      <div className="order-1 md:order-2 md:col-span-2 relative flex justify-center items-center">
        <div className="flex flex-row justify-between items-center my-4">
          <img src={Logo} alt="Emirates Airlines Logo" className="w-8 h-8" />
          <span className="">
            {airlineDesignator} {flightNumber}
          </span>
        </div>
        <div
          className={`ribbon absolute ${statusCodeClass} px-3 py-1 top-0 -right-[55px] text-white`}
          role={"status"}>
          <span>{statusCodeText}</span>
          <div className="w-3 overflow-hidden inline-block absolute -bottom-4">
            <div className="h-4 bg-gray-500 rotate-45 transform origin-top-right"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FlightRoute;
