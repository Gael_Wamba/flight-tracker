# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

- Quick summary
  --- This repository consist of a responsive web application that allows users to efficiently track the status of a flight by providing the origin, destination and departure date
- Version
  --- 1.0.0

### How do I get set up?

- Clone the repository
- Make sure your port 3000 is free
- run "npm run start"
- Your frontend app should be ready

### Assumptions?

1. The flight status cors were enabled to accept external calls from unknown clients
   I had to create a backend service to have a secure connection between my frontend and backend.
2. Flight had only one route
   Which was not the case I had to handle the case of having multiple routes

### What to expect?

- Please look at the images included in the project root folder
