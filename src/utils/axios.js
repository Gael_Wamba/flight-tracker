import axios from "axios";

axios.defaults.baseURL = 'http://127.0.0.1:3001/api/';
axios.defaults.headers.get["Access-Control-Allow-Origin"] = "*";

export default axios;