import GreenPlane from "../../assets/images/green-plane.png";

interface progressBarProps {
  progressPercentage: number;
}
const ProgressBar = ({ progressPercentage }: progressBarProps) => {
  if (progressPercentage !== 0 && progressPercentage < 14) {
    progressPercentage = 14;
  }
  return (
    <div
      className={`h-1 w-full md:w-96 sm:w-48 bg-gray-300 ${
        progressPercentage === 0 && "relative"
      }`}>
      <div
        style={{ width: `${progressPercentage}%` }}
        className={`h-full bg-green-700 ${
          progressPercentage === 0 ? "" : "relative"
        }`}>
        <img
          src={GreenPlane}
          alt="green-plane.png"
          className={`absolute ${
            progressPercentage === 0
              ? "-top-[26px] -left-[58px]"
              : "-top-[26px] -right-2 "
          } h-14 w-14`}
        />
      </div>
    </div>
  );
};

export default ProgressBar;
