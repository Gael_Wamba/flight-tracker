import { useEffect, useState } from "react";
import axios from "./utils/axios";
import Flight, { FlightData } from "./components/Flight/Flight";
import FlightSearch, { Airport } from "./components/FlightSearch/FlightSearch";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Suspense } from "react";
import moment from "moment";

const initialFlightSearchValues = {
  leavingFrom: {
    code: "",
    name: "",
  },
  goingTo: {
    code: "",
    name: "",
  },
  departing: "",
};
const initialFlightDataValues: FlightData[] = [];
const initialAirportValues: Airport[] = [];

function App() {
  const [flightSearch, setFlightSearch] = useState(initialFlightSearchValues);
  const [flightData, setFlightData] = useState(initialFlightDataValues);
  const [airports, setAirports] = useState(initialAirportValues);
  const [noResult, setNoResult] = useState(false);

  useEffect(() => {
    axios
      .get("airports")
      .then((res) => {
        setAirports(res.data);
      })
      .catch((error) => {
        toast.error(error.response.data);
      });
  }, []);

  const handleSearchAirportChange = (item: any, event: any) => {
    setFlightSearch({
      ...flightSearch,
      [event.name]: item,
    });
  };

  const handleSearchDateChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setFlightSearch({
      ...flightSearch,
      [event.target.name]: event.target.value,
    });
  };

  const handleViewResult = () => {
    if (
      !flightSearch.leavingFrom ||
      flightSearch.leavingFrom.code === "" ||
      flightSearch.leavingFrom.code === null
    ) {
      toast.error("Please select the departure airport.");
      return false;
    }

    if (
      !flightSearch.goingTo ||
      flightSearch.goingTo.code === "" ||
      flightSearch.goingTo.code === null
    ) {
      toast.error("Please select the arrival airport.");
      return false;
    }

    if (
      !flightSearch.departing ||
      flightSearch.departing === "" ||
      flightSearch.departing === null
    ) {
      toast.error("Please enter the departing date ");
      return false;
    }

    let departureDate = flightSearch.departing;
    let origin = flightSearch.leavingFrom && flightSearch.leavingFrom.code;
    let destination = flightSearch.goingTo && flightSearch.goingTo.code;

    let url =
      "flight-status?departureDate=" +
      departureDate +
      "&origin=" +
      origin +
      "&destination=" +
      destination;

    axios
      .get(url)
      .then((res) => {
        setFlightData(res.data);
        if (res.data === "") {
          setNoResult(true);
        } else {
          setNoResult(false);
        }
      })
      .catch((error) => {
        toast.error(error.response.data);
      });
  };

  return (
    <div className="mb-20">
      <div className="p-8 text-center md:text-left bg-gray-800 ">
        <div className="container max-w-6xl mx-auto text-white font-bold text-3xl">
          Emirates Flight Tracker
        </div>
      </div>

      <div className="container max-w-6xl mx-auto px-2 sm:px-6 lg:px-8 text-gray-700">
        <div className="flex flex-col text-center py-8">
          <h1 className="text-2xl  font-bold">Flight Search</h1>
          <small>
            Please select departure, arrival locations and enter departing date
          </small>
        </div>
        <Suspense fallback={<h1>Loading airports...</h1>}>
          <FlightSearch
            airports={airports}
            flightSearch={flightSearch}
            handleSearchChange={handleSearchAirportChange}
            handleSearchDateChange={handleSearchDateChange}
            handleViewResult={handleViewResult}
          />
        </Suspense>
        {flightData.length > 0 && (
          <div className="flex flex-col text-center p-8">
            <h1 className="text-4xl py-2 font-bold">Flight Status</h1>
            <span>
              Flight status from {flightSearch.leavingFrom.code} (
              {flightSearch.leavingFrom.code}) to {flightSearch.goingTo.code} (
              {flightSearch.goingTo.code})
            </span>
            <span>
              {moment(flightSearch.departing).format("ddd D MMM YYYY")}
            </span>
          </div>
        )}
        {noResult && (
          <div className="flex flex-col text-center p-8">
            <h1 className="text-2xl py-2 font-bold">
              There was no results found for this search <br />
              Please change your filter values
            </h1>
          </div>
        )}
        <div className="flex flex-col gap-8">
          <Suspense fallback={<h1>Loading flights statuses ...</h1>}>
            {flightData &&
              flightData.map((flight: FlightData) => {
                return (
                  <Flight
                    key={flight.flightId}
                    flightData={flight}
                    airports={airports}
                  />
                );
              })}
          </Suspense>
        </div>
      </div>
      <ToastContainer />
      <footer className="p-2 w-full fixed bottom-0 text-center md:text-left text-gray-800 bg-gray-300 ">
        <div className="container max-w-6xl mx-auto">
          Powered by Emirates Airlines
        </div>
      </footer>
    </div>
  );
}

export default App;
