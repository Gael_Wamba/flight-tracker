import Select from "../Select/Select";

export interface flightSearchProps {
  flightSearch: {
    leavingFrom: {
      code: string;
      name: string;
    };
    goingTo: {
      code: string;
      name: string;
    };
    departing: string;
  };
  airports: Array<Airport>;
  handleSearchDateChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleSearchChange: (
    item: any,
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  handleViewResult: React.MouseEventHandler<HTMLButtonElement>;
}

export interface Airport {
  code: string;
  name: string;
}

const FlightSearch = ({
  airports,
  flightSearch,
  handleSearchChange,
  handleSearchDateChange,
  handleViewResult,
}: flightSearchProps) => {
  return (
    <div className="grid grid-rows-1 grid-flow-row md:grid-cols-7 md:grid-flow-row gap-2 my-8">
      <div className="md:col-span-6 flex flex-col md:flex-row justify-between gap-4">
        <div className="w-full flex flex-col">
          <Select
            name="leavingFrom"
            label="Leaving From"
            value={flightSearch.leavingFrom}
            getOptionLabel={(airport: Airport) => airport.name}
            getOptionValue={(airport: Airport) => airport.code}
            options={airports}
            onHandleChange={(item: any, event: any) =>
              handleSearchChange(item, event)
            }
          />
        </div>
        <div className="w-full flex flex-col">
          <Select
            name="goingTo"
            label="Going to"
            value={flightSearch.goingTo}
            getOptionLabel={(airport: Airport) => airport.name}
            getOptionValue={(airport: Airport) => airport.code}
            options={airports}
            onHandleChange={(item: any, event: any) =>
              handleSearchChange(item, event)
            }
          />
        </div>
        <div className="w-full flex flex-col">
          <label>Departing</label>
          <input
            type={"date"}
            name={"departing"}
            value={flightSearch.departing}
            onChange={handleSearchDateChange}
            className="p-2 w-full bg-white text-gray-600 border border-gray-200 rounded-sm"
          />
        </div>
      </div>
      <div className="w-full flex flex-col justify-end">
        <label></label>
        <button
          className="bg-red-600 p-2 text-white rounded-sm shadow-lg hover:bg-red-500 cursor-pointer"
          onClick={handleViewResult}>
          View Result
        </button>
      </div>
    </div>
  );
};

export default FlightSearch;
