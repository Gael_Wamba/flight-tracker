import { Airport } from "../FlightSearch/FlightSearch";
import FlightRoute from "../FlightRoute/FlightRoute";

export interface flightProps {
  flightData: FlightData;
  airports: Array<Airport>;
}

export interface FlightData {
  airlineDesignator: string;
  flightNumber: string;
  flightId: string;
  flightDate: Date;
  flightRoute: [
    {
      originActualAirportCode: string;
      destinationActualAirportCode: string;
      statusCode: string;
      flightPosition: number;
      departureTime: {
        schedule: Date;
        estimated: Date;
        actual: Date;
      };
      arrivalTime: {
        schedule: Date;
        estimated: Date;
        actual: Date;
      };
      operationalUpdate: {
        lastUpdated: Date;
      };
    }
  ];
}

const Flight = ({ flightData, airports }: flightProps) => {
  return (
    <div className="border border-gray-200 shadow-md rounded-sm p-10">
      {flightData.flightRoute.map((flightRoute, index) => {
        return (
          <FlightRoute
            key={index}
            flightRoute={flightRoute}
            airports={airports}
            airlineDesignator={flightData.airlineDesignator}
            flightNumber={flightData.flightNumber}
          />
        );
      })}
    </div>
  );
};

export default Flight;
