import ReactSelect from "react-select";

interface selectProps {
  name: string;
  label: string;
  placeholder?: string;
  options: any;
  value: any;
  onHandleChange: any;
  getOptionLabel: any;
  getOptionValue: any;
}
const Select = ({
  name,
  label,
  options,
  value,
  onHandleChange,
  getOptionLabel,
  getOptionValue,
  placeholder,
}: selectProps) => {
  return (
    <div className="flex flex-col gap-1">
      <span>{label}</span>
      <ReactSelect
        name={name}
        options={options}
        value={value}
        placeholder={placeholder ? placeholder : label}
        onChange={onHandleChange}
        getOptionLabel={getOptionLabel}
        getOptionValue={getOptionValue}
      />
    </div>
  );
};

export default Select;
