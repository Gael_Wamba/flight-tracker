import { render, screen } from "@testing-library/react";
import App from "./App";

test("it should check if text is in the document", () => {
  render(<App />);
  const linkElement = screen.getByText(
    /Please select departure, arrival locations and enter departing date/i
  );
  expect(linkElement).toBeInTheDocument();
});

test("should check if background is green when status code equals to ARVD", () => {
  render(
    <div
      className={`ribbon absolute bg-arvd px-3 py-1 top-0 -right-[55px] text-white`}>
      <span>Arrived</span>
      <div className="w-3 overflow-hidden inline-block absolute -bottom-4">
        <div className="h-4 bg-gray-500 rotate-45 transform origin-top-right"></div>
      </div>
    </div>
  );

  expect(screen.getByRole("status")).toHaveClass("bg-arvd");
});
